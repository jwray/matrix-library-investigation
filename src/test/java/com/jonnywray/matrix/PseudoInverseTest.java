package com.jonnywray.matrix;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrix;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularValueDecomposition;
import org.ejml.simple.SimpleMatrix;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;


/**
 * Tests of the various libraries calculating pseudo inverse.
 *
 * @author Jonny Wray
 */
public class PseudoInverseTest extends AbstractMatrixTest {

    @Test(dataProvider = "mtjExamples")
    public void testMTJ(DenseMatrix matrix, SimpleMatrix expectedInverse){
        PseudoInverseMTJ pseudoInverseMTJ = new PseudoInverseMTJ();
        Matrix pseudoInverse = pseudoInverseMTJ.pseudoInverse(matrix);
        assertMatrixEquals(pseudoInverse, expectedInverse, "MTJ values are incorrect");
    }

    @Test(dataProvider = "ejmlExamples")
    public void testEJML(SimpleMatrix matrix, SimpleMatrix expectedInverse){
        SimpleMatrix pseudoInverse = matrix.pseudoInverse();
        assertMatrixEquals(pseudoInverse, expectedInverse, "EJML values are incorrect");
    }

    @Test(dataProvider = "commonsMathsExamples")
    public void testCommonsMath(RealMatrix matrix, SimpleMatrix expectedInverse){
        SingularValueDecomposition decomposition = new SingularValueDecomposition(matrix);
        RealMatrix pseudoInverse = decomposition.getSolver().getInverse();
        assertMatrixEquals(pseudoInverse, expectedInverse, "Commons Maths values are incorrect");
    }

    @DataProvider(name="mtjExamples")
    protected Object[][] mtjExamples() throws IOException {
        return new Object[][] {
                {testMTJMatrix(), expectedInverse()}
        };
    }

    @DataProvider(name="ejmlExamples")
    protected Object[][] ejmlExamples() throws IOException {
        return new Object[][] {
                {testEJMLMatrix(), expectedInverse()}
        };
    }

    @DataProvider(name="commonsMathsExamples")
    protected Object[][] commonsMathsExamples() throws IOException {
        return new Object[][] {
                {testCommonsMathMatrix(), expectedInverse()}
        };
    }

    // Same simple test matrix in different forms along with expected pseudo-inverse from
    // http://www.itl.nist.gov/div898/software/dataplot/refman2/auxillar/pseudinv.htm

    private SimpleMatrix expectedInverse(){
        SimpleMatrix matrix = new SimpleMatrix(5, 4);
        matrix.setRow(0, 0, new double[]{0.0032, -0.2179, 0.2005, 0.0298});
        matrix.setRow(1, 0, new double[]{-0.1124, 0.1551, -0.1195, 0.0757});
        matrix.setRow(2, 0, new double[]{0.0101, 0.0890, -0.0397, -0.0401});
        matrix.setRow(3, 0, new double[]{0.0390, 0.0506, 0.0154, -0.0818});
        matrix.setRow(4, 0, new double[]{0.0877, -0.0963, -0.0478, 0.0499});
        return matrix.transpose();
    }

    private DenseMatrix testMTJMatrix(){
        DenseMatrix matrix = new DenseMatrix(5, 4);
        matrix.set(0, 0, 16);
        matrix.set(0, 1, 16);
        matrix.set(0, 2, 19);
        matrix.set(0, 3, 21);
        matrix.set(1, 0, 14);
        matrix.set(1, 1, 17);
        matrix.set(1, 2, 15);
        matrix.set(1, 3, 22);
        matrix.set(2, 0, 24);
        matrix.set(2, 1, 23);
        matrix.set(2, 2, 21);
        matrix.set(2, 3, 24);
        matrix.set(3, 0, 18);
        matrix.set(3, 1, 17);
        matrix.set(3, 2, 16);
        matrix.set(3, 3, 15);
        matrix.set(4, 0, 18);
        matrix.set(4, 1, 11);
        matrix.set(4, 2, 9);
        matrix.set(4, 3, 18);
        return matrix;
    }

    private SimpleMatrix testEJMLMatrix(){
        SimpleMatrix matrix = new SimpleMatrix(5, 4);
        matrix.setRow(0, 0, new double[]{16, 16, 19, 21});
        matrix.setRow(1, 0, new double[]{14, 17, 15, 22});
        matrix.setRow(2, 0, new double[]{24, 23, 21, 24});
        matrix.setRow(3, 0, new double[]{18, 17, 16, 15});
        matrix.setRow(4, 0, new double[]{18, 11, 9, 18});
        return matrix;
    }

    private RealMatrix testCommonsMathMatrix(){
        RealMatrix matrix = new Array2DRowRealMatrix(5, 4);
        matrix.setRow(0, new double[]{16, 16, 19, 21});
        matrix.setRow(1, new double[]{14, 17, 15, 22});
        matrix.setRow(2, new double[]{24, 23, 21, 24});
        matrix.setRow(3, new double[]{18, 17, 16, 15});
        matrix.setRow(4, new double[]{18, 11, 9, 18});
        return matrix;
    }

}
