package com.jonnywray.matrix;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrix;
import org.ejml.simple.SimpleMatrix;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Test the multi-threading behaviour of the pseudo-inverse calculations. Attempt to isolate issues observed with
 * certain MTJ configurations.
 *
 * @author Jonny Wray
 */
public class PseudoInverseMultiThreadedTest extends AbstractMatrixTest{

    private static final int REPEATS = 40;
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(10);

    private static final String BLAS_KEY =  "com.github.fommil.netlib.BLAS";
    private static final String LAPACK_KEY =  "com.github.fommil.netlib.LAPACK";
    private static final String ARPACK_KEY =  "com.github.fommil.netlib.ARPACK";

    private static final String BLAS_F2J_VALUE = "com.github.fommil.netlib.F2jBLAS";
    private static final String LAPACK_F2J_VALUE = "com.github.fommil.netlib.F2jLAPACK";
    private static final String ARPACK_F2J_VALUE = "com.github.fommil.netlib.F2jARPACK";

    private static final String BLAS_SYSTEM_VALUE = "com.github.fommil.netlib.NativeSystemBLAS";
    private static final String LAPACK_SYSTEM_VALUE = "com.github.fommil.netlib.NativeSystemLAPACK";
    private static final String ARPACK_SYSTEM_VALUE = "com.github.fommil.netlib.NativeSystemARPACK";

    private static final String BLAS_REFERENCE_VALUE = "com.github.fommil.netlib.NativeRefBLAS";
    private static final String LAPACK_REFERENCE_VALUE = "com.github.fommil.netlib.NativeRefLAPACK";
    private static final String ARPACK_REFERENCE_VALUE = "com.github.fommil.netlib.NativeRefARPACK";


    @BeforeClass
    public void initialize(){
        /*
         These flag attempt to load system libraries included on the OS and possibly CPU optimized. Tested configurations;

         OSX: works
         Windows: not tested
         Linux:
              LAPACK/BLAS 3 (https://packages.debian.org/wheezy/liblapack3) works
              ATLAS (https://packages.debian.org/wheezy/libatlas3-base) fails
          */
        //System.setProperty(BLAS_KEY, BLAS_SYSTEM_VALUE);
        //System.setProperty(LAPACK_KEY, LAPACK_SYSTEM_VALUE);
        //System.setProperty(ARPACK_KEY, ARPACK_SYSTEM_VALUE);

        /*
         These flags force the loading of the native libraries included with MTJ

         They appear to fail on all OS
          */
        System.setProperty(BLAS_KEY, BLAS_REFERENCE_VALUE);
        System.setProperty(LAPACK_KEY, LAPACK_REFERENCE_VALUE);
        System.setProperty(ARPACK_KEY, ARPACK_REFERENCE_VALUE);

        /*
         These flags force the loading of the pure Java libraries (from f2j) included with MTJ and always work
          */
        // System.setProperty(BLAS_KEY, BLAS_F2J_VALUE);
        // System.setProperty(LAPACK_KEY, LAPACK_F2J_VALUE);
        // System.setProperty(ARPACK_KEY, ARPACK_F2J_VALUE);
    }


    /**
     * Test method to illustrate issue with MTJ calculations in a multi-threaded environment. Values are calculated
     * in the main thread and then multiple tasks are launched using a background thread pool and the values obtained
     * from those compared to the values obtained from the main thread
     *
     * @param matrix the matrix to test
     */
    @Test(singleThreaded = true, dataProvider = "mtjLargerExamples")
    public void testThreadSafetyMTJWithoutSharing(final DenseMatrix matrix) throws InterruptedException, ExecutionException{
        PseudoInverseMTJ initialPseudoInverseMTJ = new PseudoInverseMTJ();
        Matrix expectedInverse = initialPseudoInverseMTJ.pseudoInverse(matrix);

        SimpleMatrix ejml = convert(matrix);
        SimpleMatrix ejmlInverse = ejml.pseudoInverse();
        assertMatrixEquals(expectedInverse, ejmlInverse, "MTJ and EJML do not agree");


        CompletionService<Matrix> analysisCompletionService = new ExecutorCompletionService<>(EXECUTOR_SERVICE);
        for (int i=0; i<REPEATS; i++){
            Callable<Matrix> task = new Callable<Matrix>() {

                @Override
                public Matrix call() throws Exception {
                    long tic = System.currentTimeMillis();
                    DenseMatrix copy = matrix.copy();
                    PseudoInverseMTJ pseudoInverseMTJ = new PseudoInverseMTJ();
                    Matrix inverse = pseudoInverseMTJ.pseudoInverse(copy);
                    long toc = System.currentTimeMillis();
                    System.out.println("MTJ took "+Long.toString(toc-tic)+" ms");
                    return inverse;
                }
            };
            analysisCompletionService.submit(task);
        }
        for (int i=0; i<REPEATS; i++){
            Matrix results = analysisCompletionService.take().get();
            assertMatrixEquals(results, expectedInverse, "Repeat "+i+" failed on multi-threaded test");
        }
    }

    /**
     * Test method to illustrate issue with MTJ calculations in a multi-threaded environment. Values are calculated
     * in the main thread and then multiple tasks are launched using a background thread pool and the values obtained
     * from those compared to the values obtained from the main thread
     *
     * @param matrix the matrix to test
     */
    @Test(singleThreaded = true, dataProvider = "mtjLargerExamples")
    public void testThreadSafetyMTJWithSharing(final DenseMatrix matrix) throws InterruptedException, ExecutionException{
        PseudoInverseMTJ initialPseudoInverseMTJ = new PseudoInverseMTJ();
        Matrix expectedInverse = initialPseudoInverseMTJ.pseudoInverse(matrix);

        SimpleMatrix ejml = convert(matrix);
        SimpleMatrix ejmlInverse = ejml.pseudoInverse();
        assertMatrixEquals(expectedInverse, ejmlInverse, "MTJ and EJML do not agree");


        final PseudoInverseMTJ pseudoInverseMTJ = new PseudoInverseMTJ();
        CompletionService<Matrix> analysisCompletionService = new ExecutorCompletionService<>(EXECUTOR_SERVICE);
        for (int i=0; i<REPEATS; i++){
            Callable<Matrix> task = new Callable<Matrix>() {

                @Override
                public Matrix call() throws Exception {
                    long tic = System.currentTimeMillis();
                    DenseMatrix copy = matrix.copy();
                    Matrix inverse = pseudoInverseMTJ.pseudoInverse(copy);
                    long toc = System.currentTimeMillis();
                    System.out.println("MTJ took "+Long.toString(toc-tic)+" ms");
                    return inverse;
                }
            };
            analysisCompletionService.submit(task);
        }
        for (int i=0; i<REPEATS; i++){
            Matrix results = analysisCompletionService.take().get();
            assertMatrixEquals(results, expectedInverse, "Repeat " + i + " failed on multi-threaded test");
        }
    }


    private SimpleMatrix convert(DenseMatrix matrix){

        SimpleMatrix ejml = new SimpleMatrix(matrix.numRows(), matrix.numColumns());
        for(int i=0;i<matrix.numRows();i++){
            for(int j=0;j<matrix.numColumns();j++){
                ejml.set(i, j, matrix.get(i, j));
            }
        }
        return ejml;
    }


    @DataProvider(name="mtjLargerExamples")
    protected Object[][] mtjLargerExamples() throws IOException {
        return new Object[][] {
                {largeRandom()}
        };
    }

    private Random random = new Random();

    private Matrix largeRandom(){

        DenseMatrix matrix = new DenseMatrix(500, 500);
        for(int i=0; i<500; i++){
            for(int j=0;j<500;j++){
                matrix.set(i, j, random.nextDouble());
            }
        }
        return matrix;
    }


}
