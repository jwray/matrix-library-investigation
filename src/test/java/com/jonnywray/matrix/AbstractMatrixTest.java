package com.jonnywray.matrix;

import no.uib.cipr.matrix.Matrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.ejml.simple.SimpleMatrix;

import static org.testng.Assert.assertEquals;


/**
 * Created by jonny.wray on 13/04/2015.
 */
public abstract class AbstractMatrixTest {

    private static final double ACCURACY = 0.001;

    protected void assertMatrixEquals(Matrix matrix, SimpleMatrix expected, String message){
        for(int i=0;i<expected.numRows(); i++){
            for(int j=0;j<expected.numCols(); j++){
                assertEquals(matrix.get(i, j), expected.get(i, j), ACCURACY, message);
            }
        }
    }

    protected void assertMatrixEquals(SimpleMatrix matrix, SimpleMatrix expected, String message){
        for(int i=0;i<expected.numRows(); i++){
            for(int j=0;j<expected.numCols(); j++){
                assertEquals(matrix.get(i, j), expected.get(i, j), ACCURACY, message);
            }
        }
    }

    protected void assertMatrixEquals(RealMatrix matrix, SimpleMatrix expected, String message){
        for(int i=0;i<expected.numRows(); i++){
            for(int j=0;j<expected.numCols(); j++){
                assertEquals(matrix.getEntry(i, j), expected.get(i, j), ACCURACY, message);
            }
        }
    }

    protected void assertMatrixEquals(Matrix matrix, Matrix expected, String message){
        for(int i=0;i<expected.numRows(); i++){
            for(int j=0;j<expected.numColumns(); j++){
                /*
                if(Math.abs(matrix.get(i,j) - expected.get(i, j)) > ACCURACY){
                    System.out.println(matrix.get(i, j) + " vs " + expected.get(i, j)+ " " + Math.abs(matrix.get(i,j) - expected.get(i, j)));
                    double normA = matrix.norm(Matrix.Norm.Frobenius);
                    double normB = matrix.norm(Matrix.Norm.Frobenius);
                    System.out.println("Norm: "+normA+" vs "+normB);
                }
                */
                assertEquals(matrix.get(i, j), expected.get(i, j), ACCURACY, message);
            }
        }
    }

}
