package com.jonnywray.matrix;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.NotConvergedException;
import no.uib.cipr.matrix.SVD;

/**
 * Calculation of the pseudo-inverse via singular value decomposition of the MTJ library
 *
 * @author Jonny Wray
 */
public class PseudoInverseMTJ {

    public static double EPS = Math.pow(2.0D, -52.0D);

    public synchronized Matrix pseudoInverse(DenseMatrix original){
        return pseudoInverse(original, 1E-10);
    }

    public synchronized Matrix pseudoInverse(DenseMatrix original, double effectiveZero) {
        try {

            SVD internalSVD = SVD.factorize(original);

            Matrix v = internalSVD.getVt().transpose();
            Matrix uTranspose = internalSVD.getU().transpose();
            double[] s = internalSVD.getS();

            Matrix values = new DenseMatrix(v.numColumns(), uTranspose.numRows());
            for (int i = 0; i < s.length; i++) {
                double singularValue = s[i];
                double svinv;
                if (singularValue <= effectiveZero) {
                    svinv = 0.0D;
                } else {
                    svinv = 1.0D / singularValue;
                }
                values.set(i, i, svinv);
            }
            Matrix finalMatrixOne = v.mult(values, new DenseMatrix(v.numRows(), values.numColumns()));
            return finalMatrixOne.mult(uTranspose, new DenseMatrix(finalMatrixOne.numRows(), uTranspose.numColumns()));
        }
        catch (NotConvergedException e){
            e.printStackTrace();
            throw new IllegalArgumentException("Cannot calculate pseudo-inverse", e);
        }
    }

    public synchronized Matrix pseudoInverseEJMLCopy(DenseMatrix original) {
        try {

            SVD internalSVD = SVD.factorize(original);

            Matrix v = internalSVD.getVt().transpose();
            Matrix uTranspose = internalSVD.getU().transpose();
            double[] s = internalSVD.getS();

            double maxSingular = maxsingular(original, s);
            double threshold = threshold(original, maxSingular);
            Matrix values = new DenseMatrix(v.numColumns(), uTranspose.numRows());
            for (int i = 0; i < s.length; i++) {
                double singularValue = s[i];
                if(maxSingular != 0.0D) {
                    double svinv = (singularValue <= threshold) ? 0.0D : 1.0D / singularValue;
                    values.set(i, i, svinv);
                }
                else{
                    values.set(i, i, singularValue);
                }
            }
            Matrix finalMatrixOne = v.mult(values, new DenseMatrix(v.numRows(), values.numColumns()));
            return finalMatrixOne.mult(uTranspose, new DenseMatrix(finalMatrixOne.numRows(), uTranspose.numColumns()));
        }
        catch (NotConvergedException e){
            e.printStackTrace();
            throw new IllegalArgumentException("Cannot calculate pseudo-inverse", e);
        }
    }

    private double maxsingular(DenseMatrix original, double[] S){
        double maxSingular = 0.0D;

        int N = Math.min(original.numRows(), original.numColumns());
        for(int tau = 0; tau < N; ++tau) {
            if(S[tau] > maxSingular) {
                maxSingular = S[tau];
            }
        }
        return maxSingular;

    }

    private double threshold(DenseMatrix original, double maxSingular){
        return EPS * (double)Math.max(original.numRows(), original.numColumns()) * maxSingular;
    }
}
